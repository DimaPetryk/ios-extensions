import Foundation

extension Int {
    func toDouble() -> Double {
        Double(self)
    }
}

extension Double {
    func toInt() -> Int {
        Int(self)
    }
}

extension Int {
    func toString() -> String {
        "\(self)"
    }
}

extension Double {
    func toString() -> String {
        String(format: "%.02f", self)
    }
}
