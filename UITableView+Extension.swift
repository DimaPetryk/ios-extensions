//
//  UITableView+Extension.swift
//  FreeStuffFinder
//
//  Created by MacBook Pro on 20.08.2020.
//  Copyright © 2020 EltexSoft. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func sectionIndex(at point: CGPoint) -> Int? {
     
        var rectArray = [CGRect]()
        for i in .zero...numberOfSections - 1 {
            rectArray.append(rect(forSection: i))
        }
        
        for rect in rectArray {
            if rect.contains(point) {
                return rectArray.firstIndex(of: rect)
            }
        }
        
        return nil
    }
}

