/* Usage
 let json = "{\"hello\": \"world\"}"
 let dictFromJson = json.asDict*/
extension String {
    var asDict: [String: Any]? {
        guard let data = self.data(using: .utf8) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
    }
}

extension String {
    var asArray: [Any]? {
        guard let data = self.data(using: .utf8) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Any]
    }
}


//URL
extension String {
    var asURL: URL? {
        URL(string: self)
    }
}

/* Usage
 let htmlString = "<p>Hello, <strong>world!</string></p>"
 let attrString = htmlString.asAttributedString**/
extension String {
    var asAttributedString: NSAttributedString? {
        guard let data = self.data(using: .utf8) else { return nil }
        return try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
    }
}
