# iOS-extensions


> **Pods**

https://github.com/SwifterSwift/SwifterSwift 

> **Variables**

**Date+Format**
Simple function which will conver Date to String. 
Use https://nsdateformatter.com use for detect Date Format.

**Int+Double**
Fast converting Int->Double, Double->Int, Int->String, Double->String

**String+Extensions**
Fast converting JSON String -> Dictionary, JSON String -> Array, String -> URL,  String with HTML tags -> Attributed String

> **UI**

**UIView+Extensions**
Add border, corner radius, make rounded in one string. 

**UIView+Xib**
Simple initialization xib views. 
